@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">MODULO DE CLIENTES</div>
                <div class="col text-right">
                    <a href="{{route('crear.clientes')}}" class="btn-sm btn-primary">Nuevo Cliente</a>
</div>
                <div class="card-body">
                    
                <table class="table">
                    <thead>
                  <tr>
                    <th scope="col"># ID</th>
                    <th scope="col">nombre</th>
                    <th scope="col">apellidos</th>
                    <th scope="col">cedula</th>
                    <th scope="col">dirección</th>
                    <th scope="col">teléfono</th>
                    <th scope="col">fecha nacimiento</th>
                    <th scope="col">email</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($cliente as $item)
                  <tr>
                    <th scope="row">{{$item->id}}</th>
                    <td>{{$item->nombre}}</td>
                    <td>{{$item->apellidos}}</td>
                    <td>{{$item->telefono}}</td>    
                    <td>{{$item->cedula}}</td>
                    <td>{{$item->direccion}}</td>
                    <td>{{$item->fecha_nacimiento}}</td>
                    <td>{{$item->email}}</td>
                  </tr>
                  @endforeach
                </tbody>
               </table>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection